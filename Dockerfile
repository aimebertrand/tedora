FROM fedora:latest

ENV TZ=Europe/Berlin

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN dnf upgrade -y && \
    dnf install -y \
    apt-utils \
    bat \
    curl \
    fd-find \
    fzf \
    git \
    git-delta \
    gnupg \
    neovim \
    openssh \
    ripgrep \
    rofi \
    the_silver_searcher \
    sudo \
    wget \
    zsh \
    zsh-autosuggestions \
    zsh-syntax-highlighting

# Replace 1000 with your user & group id
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/timu && \
    mkdir -p /etc/sudoers.d && \
    echo "timu:x:${uid}:${gid}:Timu,,,:/home/timu:/usr/bin/zsh" >> /etc/passwd && \
    echo "timu:x:${uid}:" >> /etc/group && \
    echo "timu ALL=(ALL:ALL) ALL" > /etc/sudoers.d/timu && \
    echo "timu:timu" | chpasswd && \
    chmod 0440 /etc/sudoers.d/timu

ADD dotfiles /home/timu/.dotfiles

ADD dotfiles /root/.dotfiles

RUN mkdir -p /home/timu/.config && \
    ln -s /home/timu/.dotfiles/bat /home/timu/.config/ && \
    ln -s /home/timu/.dotfiles/git/config /home/timu/.gitconfig && \
    ln -s /home/timu/.dotfiles/nvim /home/timu/.config/ && \
    ln -s /home/timu/.dotfiles/zsh/zshrc.zsh /home/timu/.zshrc

RUN chown timu:timu -R /home/timu

RUN mkdir -p /root/.config && \
    ln -s /root/.dotfiles/bat /root/.config/ && \
    ln -s /root/.dotfiles/git/config /root/.gitconfig && \
    ln -s /root/.dotfiles/nvim /root/.config/ && \
    ln -s /root/.dotfiles/zsh/zshrc.zsh /root/.zshrc

RUN nvim -E +PlugUpgrade +PlugInstall +qall || true

USER timu

RUN nvim -E +PlugUpgrade +PlugInstall +qall || true

ENV HOME /home/timu

ENV SHELL /usr/bin/zsh

ENV EDITOR /usr/bin/vim

ENV MYVIMRC $HOME/.dotfiles/nvim/init.vim

WORKDIR /home/timu

CMD ["zsh"]
