#!/bin/bash

# FUNCTION:
# ---------
# Connect/disconnect to my VPN depending on the state
# 2020-12-05

# LICENCE:
# --------
# Copyright 2020 Aimé Bertrand

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

networkService=$(scutil --nc list | grep IPSec | cut -d'"' -f2)

function fritz_status () {
    scutil --nc status "$networkService" | grep 10.9 > /dev/null 2>&1
    case $? in
	1) printf "%s is disconnected\n" "$networkService";;
	0) IPAddr=$(scutil --nc status "$networkService" | grep '0 : 10.9' | cut -d':' -f2);\
	   printf "Connected to %s with IP:%s\n" "$networkService" "$IPAddr";;
    esac
}

function fritz_connect_disconnect () {
    scutil --nc status "$networkService" | grep 10.9 > /dev/null 2>&1
    case $? in
	1) scutil --nc start "$networkService";\
	   echo "Connecting...";sleep 3;fritz_status;;
	0) scutil --nc stop "$networkService";\
	   fritz_status;;
    esac
}

case $1 in
    -s) fritz_status;;
    state) fritz_status;;
    *) fritz_connect_disconnect;;
esac

exit
