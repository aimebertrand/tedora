#!/bin/bash

# FUNCTION:
# ---------
# Create org file with my currently brew installed stuff.

# LICENCE:
# --------
# Copyright 2020 Aimé Bertrand

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# put all the brew installed stuff in a temp file
brew leaves > /private/tmp/brew-list.log
brew list --cask > /private/tmp/brew-cask-list.log

# delete the old documentation file
[ -f $HOME/Desktop/brew-installed-info.org ] && \
    /bin/rm $HOME/Desktop/brew-installed-info.org

# add org title stuff
cat << _EOF_ >> $HOME/Desktop/brew-installed-info.org
#+TITLE: INFORAMTION ABOUT BREW INSTALLED STUFF
#+CREATOR: Aimé Bertrand
#+LANGUAGE: en
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://macowners.club/css/gtd.css" />
#+STARTUP: indent content

_EOF_

echo >> $HOME/Desktop/brew-installed-info.org
echo "* FORMULAE" >> $HOME/Desktop/brew-installed-info.org

while read formula; do
    echo "** ${formula}" >> $HOME/Desktop/brew-installed-info.org
    brew info ${formula} >> $HOME/Desktop/brew-installed-info.org
done < /private/tmp/brew-list.log

echo >> $HOME/Desktop/brew-installed-info.org
echo "* CASKS" >> $HOME/Desktop/brew-installed-info.org

while read cask; do
    echo "** ${cask}" >> $HOME/Desktop/brew-installed-info.org
    brew info --cask ${cask} >> $HOME/Desktop/brew-installed-info.org
done < /private/tmp/brew-cask-list.log

sed -i '' 's/==>/***/g' $HOME/Desktop/brew-installed-info.org
