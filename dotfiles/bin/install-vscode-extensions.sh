#!/bin/bash

# FUNCTION:
# ---------
# install vscode extensions on new machines after cloning my dotfiles
# @ git@gitlab.com:aimebertrand/dotfiles.git

# LICENCE:
# --------
# Copyright 2021 Aimé Bertrand

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


function install_extensions () {
    if [ $(which code) ]
    then
        codium --install-extension aimebertrand.timu-spacegrey-vscode
        codium --install-extension bungcip.better-toml
        codium --install-extension CoenraadS.bracket-pair-colorizer-2
        codium --install-extension formulahendry.code-runner
        codium --install-extension GitLab.gitlab-workflow
        codium --install-extension golang.go
        codium --install-extension jacobdufault.fuzzy-search
        codium --install-extension kahole.magit
        codium --install-extension mattn.Lisp
        codium --install-extension ms-python.python
        codium --install-extension naumovs.color-highlight
        codium --install-extension redhat.ansible
        codium --install-extension PKief.material-icon-theme
        codium --install-extension vscode-org-mode.org-mode
        codium --install-extension vscodevim.vim
    else
        echo "'code' does not seem to be installed"
    fi
}


if [ -d ~/.dotfiles/vscode/dotvscode/extensions ]
then
    install_extensions
else
    mkdir ~/.dotfiles/vscode/dotvscode/extensions
    install_extensions
fi

exit
