local uname = io.popen("uname")
local system = uname:read("*l")
uname:close()

if system == "Darwin" then
  local os_is_dark = function()
    return (vim.call(
          'system',
          [[echo $(defaults read -globalDomain AppleInterfaceStyle \
&> /dev/null && echo 'dark' || echo 'light')]]
        )):find('dark') ~= nil
  end
  if os_is_dark() then
    vim.o.background = 'dark'
    vim.cmd([[colorscheme timu-macos-vim]])
    require('lualine').setup {
      options = { theme = 'timu_macos_dark' },
    }
  else
    vim.o.background = 'light'
    vim.cmd([[colorscheme timu-macos-vim-light]])
    require('lualine').setup {
      options = { theme = 'timu_macos_light' },
    }
  end
elseif system == "Linux" then
  vim.o.background = 'dark'
  vim.cmd([[colorscheme timu-spacegrey-vim]])
  require('lualine').setup {
    options = { theme = 'timu_spacegrey_dark' },
  }
end
