# ALIASES
# -------

## 1. DEFAULT COMMANDS MORE APPEALING & USEFUL
alias ....='cd ../../../' # Go back 3 directory levels
alias ...='cd ../../' # Go back 2 directory levels
alias ..='cd ../' # Go back 1 directory level
alias ba='exit' # Exit shell
alias c='clear' # Clear terminal display
alias cp='cp -ivp' # verbose (v) und Abfrage ob ersetzen (i)
alias grep='grep --color=yes' # Color highlighting for grep
alias ls='ls -hp --color=always' # *B Sizes (-h); / for directories (-p); color (--color)
alias la='ls -Al' # Alles bis auf . & .. anzeigen - auch versteckte Daten
alias ll='ls -l' # Alles bis auf . & .. und versteckte Daten anzeigen
alias lo='ls -l *.org' # ls -al org files in the wd
alias mkd='mkdir -pv' # Verbose and parent directories for mkdir
alias mv='mv -iv' # verbose (v) und Abfrage ob ersetzen (i)
alias nano='nano -l' # Show line numbers in nano

## 2. CUSTOM
alias cdl='curl -sS -O' # curl into file with original name
alias his='fc -l 1' # always show the whole history and the date
alias hist='fc -li 1' # always show the whole history and the date
alias vi='/usr/bin/vim'
alias vim='nvim'
alias vf='vifm ~'
alias vless="nvim -c 'set ft=man'"
alias rr='ranger --choosedir=$HOME/.dotfiles/ranger/rangerdir; \
LASTDIR=$(cat $HOME/.dotfiles/ranger/rangerdir); cd "$LASTDIR"'
