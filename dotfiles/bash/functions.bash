## FUNCTIONS

zipf () { zip -r "$1".zip "$1" ; } # to create a zip archive of a folder
trash () { command mv "$@" ~/.Trash ; } # moves a file to the macos trash
ql () { qlmanage -p "$*" >& /dev/null; } # opens any file in macos quicklook preview

# GIT_BRANCH
# get info of the current git branch (will be used in promt)
function git_branch {
    local git_status="$(git status 2> /dev/null)"
    local on_branch="On branch ([^${IFS}]*)"
    local on_commit="HEAD detached at ([^${IFS}]*)"
    if [[ $git_status =~ $on_branch ]]; then
    local branch=${BASH_REMATCH[1]}
    echo "($branch)"
    elif [[ $git_status =~ $on_commit ]]; then
    local commit=${BASH_REMATCH[1]}
    echo "($commit)"
    fi
}
