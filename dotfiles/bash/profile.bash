## BASH_PROFILE

# VARIABLES
source $HOME/.dotfiles/bash/aliases.bash # soure the aliases for the bash shell
source $HOME/.dotfiles/bash/functions.bash # soure the custom functions for the bash shell
export EDITOR=nvim

# COLORS
RED='\[$(tput setaf 1)\]'
GREEN='\[$(tput setaf 2)\]'
YELLOW='\[$(tput setaf 3)\]'
BLUE='\[$(tput setaf 4)\]'
MAGENTA='\[$(tput setaf 5)\]'
CYAN='\[$(tput setaf 6)\]'
WHITE='\[$(tput setaf 7)\]'
NEWLINE='\n'
RESET='\[\e[0m\]'
thishost=$machine
export CLICOLOR=1 # output in color

# PATHS
export PATH=$HOME/.dotfiles/bin:$PATH

# HISTORY
export HISTSIZE=100000
export HISTFILESIZE=100000
HISTFILE=$HOME/.dotfiles/bash/bash_history


## GIT-PROMPT
source $HOME/.dotfiles/git/git-prompt.sh
gitbranch='$(__git_ps1 "%s ")'

# PROMPT
PS1="${NEWLINE}"
PS1+="${MAGENTA}${gitbranch}"
PS1+="${BLUE}${thishost}${RED}:${BLUE}\W${RED}:${RESET} "

# VI-MODE
set -o vi
# for cursor shape:
# put the following in ~/.inputrc:
# set show-mode-in-prompt on
# set vi-cmd-mode-string "\1\e[2 q\2"
# set vi-ins-mode-string "\1\e[6 q\2"

# FZF
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
