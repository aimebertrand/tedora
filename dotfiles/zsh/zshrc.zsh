# ZSHRC
# -----

## 1. TRAMP FIX
[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ ' && return

## 2. PRELOADS
export EDITOR=nvim
export GIT_PAGER=delta
export MANPAGER='nvim +Man!'
export KEYTIMEOUT=1
export LANG=de_DE.utf8
export MYVIMRC=$HOME/.dotfiles/nvim/init.vim
export TERM='xterm-256color'
export VISUAL=nvim
[ -f $HOME/.dotfiles/zsh/additions.zsh ] && \
    source $HOME/.dotfiles/zsh/additions.zsh # addons, plugins & co
[ -f $HOME/.dotfiles/zsh/aliases.zsh ] && \
    source $HOME/.dotfiles/zsh/aliases.zsh # all aliases for zsh
[ -f $HOME/.dotfiles/zsh/functions.zsh ] && \
    source $HOME/.dotfiles/zsh/functions.zsh # custom functions
[ -f $HOME/projects/pdotfiles/zsh/private.zsh ] && \
    source $HOME/projects/pdotfiles/zsh/private.zsh # not for public git repo

## 4. COLORS VARIABLES
BLACK='%F{0}'
RED='%F{1}'
GREEN='%F{2}'
YELLOW='%F{3}'
BLUE='%F{4}'
MAGENTA='%F{5}'
CYAN='%F{6}'
WHITE='%F{7}'
PURPLE='%F{105}'
BROWN='%F{130}'
ORANGE='%F{9}'

## 5. OTHER VARIABLES
thishost=$(cat /etc/hostname)
user='%n'
directory='%1~'
exitcode='%(?..${RED}✘ %? )' # if %? != 0, show "✘ %?"
time='%*'

## 6. PROMPT
autoload -U colors && colors
setopt PROMPT_SUBST

if [[ $EUID -ne 0 ]]; then
    PROMPT="${MAGENTA}${gitbranch}${exitcode}${ORANGE}${thishost}${BLUE}%B:%b${ORANGE}${directory}${BLUE}%B:%b%f "
else
    PROMPT="${MAGENTA}${gitbranch}${exitcode}${RED}${thishost}%B:%b${directory}%B:%b%f "
fi
