# ZSH_ADDITIONS
# -------------

## 1. SYNTAX HIGHLIGHTING
[ -f /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && \
    source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[ -f /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && \
    source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[ -f /opt/homebrew/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && \
    source /opt/homebrew/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
typeset -A ZSH_HIGHLIGHT_STYLES # overrights for the default plugin settings
ZSH_HIGHLIGHT_STYLES[path]='none' # path highlighting

## 2. AUTOSUGGESTIONS
[ -f /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh ] && \
    source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh
[ -f /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh ] && \
    source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
[ -f /opt/homebrew/share/zsh-autosuggestions/zsh-autosuggestions.zsh ] && \
    source /opt/homebrew/share/zsh-autosuggestions/zsh-autosuggestions.zsh

## 4. CUSTOM PATH
if [[ $(arch) == 'arm64' ]]
then
    path=(
        $HOME/.dotfiles/bin
        /opt/homebrew/bin
        /opt/homebrew/sbin
        /usr/local/bin
        /usr/local/sbin
        $path
    )
    typeset -U path

    export HOMEBREW_PREFIX='/opt/homebrew'
    export HOMEBREW_CELLAR='/opt/homebrew/Cellar'
    export HOMEBREW_REPOSITORY='/opt/homebrew'
    export HOMEBREW_SHELLENV_PREFIX='/opt/homebrew'
else
    path=(
        $HOME/.dotfiles/bin
        /usr/local/bin
        /usr/local/sbin
        /opt/homebrew/bin
        /opt/homebrew/sbin
        $path
    )
    typeset -U path

    export HOMEBREW_PREFIX='/usr/local'
    export HOMEBREW_CELLAR='/usr/local/Cellar'
    export HOMEBREW_REPOSITORY='/usr/local/Homebrew'
    export HOMEBREW_SHELLENV_PREFIX='/usr/local'
fi

## 5. HISTORY
export HISTFILE=$HOME/.dotfiles/zsh/history.zsh
export HISTSIZE=1000000
export SAVEHIST=1000000
export HISTTIMEFORMAT="[%F %T] "
setopt HIST_IGNORE_ALL_DUPS # no older duplicates
setopt HIST_REDUCE_BLANKS # remove superfluous blanks
setopt HIST_IGNORE_SPACE # ignore commands with space at the beginning
setopt SHARE_HISTORY # share history between different instances of the shell
setopt INC_APPEND_HISTORY # add cmd to the history immediately
setopt EXTENDED_HISTORY # add timestamp to the history

## 6. AUTOCOMPLETION
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
_comp_options+=(globdots) # Include dot files
setopt auto_menu # automatically use menu completion
setopt always_to_end # move cursor to end if word had one match
# case insensitive path-completion
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*'

## 7. CHANGE CURSOR SHAPE FOR DIFFERENT VI MODES.
# change cursor shape in iTerm2
function zle-keymap-select zle-line-init {
    case $KEYMAP in
    vicmd)      echo -ne '\e[1 q';; # block cursor
    viins|main) echo -ne '\e[5 q';; # line cursor
    esac
    zle reset-prompt
    zle -R
}

# reset to block after running command
function zle-line-finish {
    echo -ne '\e[1 q' # block cursor
}

zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select

## 8. VI-MODE
bindkey -v
bindkey '^?' backward-delete-char # fix backspace bug when vi modes

## 9. EDIT PROMT CMD IN $EDITOR
autoload -z edit-command-line
zle -N edit-command-line
bindkey -M vicmd ' ' edit-command-line # with <space> key

## 10. GETTEXT STUFF
# instructions by homebrew
export LDFLAGS="-L/usr/local/opt/gettext/lib"
export CPPFLAGS="-I/usr/local/opt/gettext/include"
export LC_ALL=en_GB.UTF-8

## 11. FZF PLUGIN
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_COMMAND="fd -p -i -H -L -E 'Library/*' -E 'Pictures/Photos Library.photoslibrary/*'"

# colors for fzf (spacegrey inspired)
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS"
--color=dark
--color=fg:-1,bg:-1,hl:#b48ead,fg+:#ffffff,bg+:#4f5b66,hl+:#b48ead
--color=info:#a3be8c,prompt:#4db5bd,pointer:#bf616a,marker:#ecbe7b,spinner:#4db5bd,header:#4db5bd
"
export FZF_COMPLETION_TRIGGER='##'

## 12. SHOW PATH IN ITERM TITLE BAR
if [ $ITERM_SESSION_ID ]; then
    precmd() {
    echo -ne "\033]1;${PWD}\007"
    }
fi

## 13. FOR PDF TOOLS IN EMACS
export PKG_CONFIG_PATH="/usr/local/opt/qt/lib/pkgconfig"

## 14. PIPENV SETTINGS
export PIPENV_VENV_IN_PROJECT=1

## 15. CUSTOM LSCOLORS
# see: https://apple.stackexchange.com/a/282189
export LS_COLORS="di=34:ln=35:so=31:pi=33:ex=32:bd=36:cd=33:su=31:sg=36:tw=32:ow=33"

## 16. BAT COLORS
export BAT_THEME=timu-spacegrey

## 17. ANALYTICS FOR HOMBREW
# https://github.com/Homebrew/brew/issues/142
export HOMEBREW_NO_ANALYTICS=1

## 18. SSH AGENT
eval $(ssh-agent -s) > /dev/null

## 19. GIT-PROMPT
source $HOME/.dotfiles/git/git-prompt.sh
GIT_PS1_SHOWDIRTYSTATE=true
GIT_PS1_SHOWSTASHSTATE=true
GIT_PS1_SHOWUNTRACKEDFILES=true
gitbranch='$(__git_ps1 "%s ")'
